<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function getStatus($id) {
		switch($id) {
			case 1 :
			$status = 'Active';
			break;
			case 0 :
			$status = 'Inactive';
			break;
		}
		return $status;
	}

	function getRoleName($id) {
		switch($id) {
			case 1 :
			$role = 'Admin';
			break;
			case 2 :
			$role = 'Pendaftar';
			break;
			case 3 :
			$role = 'Pelulus';
			break;
		}
		return $role;
	}

	function convertjenis($jenis) {
		switch($jenis) {
			case 'tender' :
			$jenis = 'TENDER';
			break;
			case 'sebutharga' :
			$jenis = 'SEBUT HARGA';
			break;
			case 'ebidding' :
			$jenis = 'EBIDDING';
			break;
		}
		return $jenis;
	}

	function getTenderStatus($status) {
		switch($status) {
			case '1':
			$status = 'BARU';
			break;
			case '2':
			$status = 'IKLAN';
			break;
			case '3':
			$status = 'PENILAIAN';
			break;
			case '4':
			$status = 'SELESAI';
			break;
			case '5':
			$status = 'BATAL';
			break;
		}
		return $status;
	}
