<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('m_admin');
    }

	function index() {
		redirect('admin/pengguna', 'refresh');
	}

	function pengguna() {

		if($this->m_pengguna->check_login()) {

			$data['title'] = 'Pengguna';
			$data['user'] = $this->session->userdata('logged_in');
			$data['pengguna'] = $this->m_pengguna->getAllUsers();

			$this->load->view('v_header', $data);
			$this->load->view('v_menu', $data);
			$this->load->view('v_senarai_pengguna', $data);
			$this->load->view('v_footer', $data);

		}

	}

	function daerah() {
		if($this->m_pengguna->check_login()) {

			$data['title'] = 'Daerah';
			$data['user'] = $this->session->userdata('logged_in');
			//$data[''] = $this->m_admin->

			$this->load->view('v_header', $data);
			$this->load->view('v_menu', $data);
			$this->load->view('v_senarai_daerah', $data);
			$this->load->view('v_footer', $data);
		}
	}

	function get_daerah_from_negeri($negeri) {
		$daerah = array();
		$daerah = $this->m_admin->getDaerahFromNegeri($negeri);
		if($daerah == 0) {
			$dropdown_daerah = "Tiada daerah";
		}

		else {
			// $table_daerah = "<table class='table'>";
			// $table_daerah .= "<thead><tr><th>Daerah</th></tr></thead>";
			// $table_daerah .= "<tbody>";
			// 	foreach($daerah as $d) {
			// 		$table_daerah .= "<tr>";
			// 		$table_daerah .= "<td>" . $d['nama_daerah'] . "</td>";
			// 		$table_daerah .= "</tr>";
			// 	}
			// $table_daerah .= "</tbody>";
			// $table_daerah .= "</table>";

			$dropdown_daerah = form_dropdown('daerah_id', $daerah, "", "class='form-control chosen-select'");
		}

		echo $dropdown_daerah;
		//echo $table_daerah;

	}

}