<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		   if($this->m_pengguna->check_login())
   		{

   			$data['title'] = 'Dashboard';
   			$data['user'] = $this->session->userdata('logged_in');
   			
   			$this->load->view('v_header', $data);
            $this->load->view('v_menu', $data);
   			$this->load->view('v_dashboard', $data);
   			$this->load->view('v_footer', $data);

   		}
			
	}

   public function logout() {

      $this->session->unset_userdata('logged_in');
      $this->session->sess_destroy();
      redirect('main', 'refresh');
 
   }

}