<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

   function __construct()
   {
      parent::__construct();
   }

	public function index()
	{
      $data['message'] = '';
      $data['title'] = 'Login';
      $this->load->view('v_header', $data);
      $this->load->view('v_login', $data);
      $this->load->view('v_footer', $data);
	}

   public function validate() {

      if($this->input->post()) {
         $result = $this->m_pengguna->login();

         if($result) {
            //die();
            $sess_array = array();
               foreach($result as $row) {
                  $sess_array = array(
                     'id' => $row->id,
                     'username' => $row->username,
                     'role' => $row->role,
                     'seksyen' => $row->seksyen
                  );

               $this->session->set_userdata('logged_in', $sess_array);

               }

               redirect('main', 'refresh');

         }

         else {
            $data['message'] = 'Username atau Password Salah';
         }

         $data['title'] = 'Login';
         $this->load->view('v_header', $data);
         $this->load->view('v_login', $data);
         $this->load->view('v_footer', $data);

      }
   }

   public function dummy($pwd) {
      $this->load->library('encrypt');
      
      $pwd2 = $this->encrypt->encode($pwd);
      echo $pwd2;
   }

}