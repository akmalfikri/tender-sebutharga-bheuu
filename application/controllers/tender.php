<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tender extends CI_Controller {

	function index() {
		redirect('tender/senarai', 'refresh');
	}

	function senarai($jenis = '', $status = '') {

		if($this->m_pengguna->check_login()) {
			
			if($jenis == '') {
				redirect('tender/senarai/all/baru', 'refresh');
			}

			switch($status) {
				case 'baru' :
				$status = 1;
				break;
				case 'penilaian' :
				$status = 3;
				break;
				case 'selesai' :
				$status = 4;
				break;
				case 'iklan' :
				$status = 2;
				break;
				case 'batal' :
				$status = 5;
				break;
			}

			switch($jenis) {
				case 'all' : 
				$title = 'Senarai';
				break;
				case 'tender' :
				$title = 'Senarai Tender';
				break;
				case 'sebutharga' : 
				$title = 'Senarai Sebutharga';
				break;
				case 'ebidding' : 
				$title = 'Senarai Ebidding';
				break;
			}


			$data['title'] = $title;
			$data['user'] = $this->session->userdata('logged_in');
			
			$count_baru = $this->m_tender->getNumTenders($jenis ,$status);
			//$print_r($count_baru);

	        $perpage = 10;
	        if ($count_baru > $perpage) {
	            $this->load->library('pagination');
	            $config['base_url'] = base_url('tender/senarai/'.$jenis.'/'.$status);
	            $config['total_rows'] = $count_baru;
	            $config['per_page'] = $perpage;
	            $config['uri_segment'] = 5;
	            $this->pagination->initialize($config);
	            $data['paging'] = $this->pagination->create_links();
	            $offset = $this->uri->segment(5);
	        } else {
	            $data['paging'] = '';
	            $offset = 0;
	        }

	        $data['jenis'] = $jenis;
	        $data['status'] = $status;
	        $data['offset'] = $offset;
	        $data['tender'] = $this->m_tender->getAllTenders($jenis, $status, $perpage, $offset);

			$this->load->view('v_header', $data);
			$this->load->view('v_menu', $data);
			$this->load->view('v_senarai_tender', $data);
			$this->load->view('v_footer', $data);

		}

	}

	function baru() {

		if($this->m_pengguna->check_login()) {

			$user = $this->session->userdata('logged_in');
			$seksyen = $user['seksyen'];

			$data['officers'] = $this->m_tender->getPegawai($seksyen);
			$data['classes'] = $this->m_tender->getClasses($seksyen);

			if($this->input->post()) {
				$this->m_tender->save_tender();
			}

			$data['title'] = 'Tender / Sebutharga Baru';
			$data['user'] = $this->session->userdata('logged_in');
			$this->load->view('v_header', $data);
			$this->load->view('v_menu', $data);
			$this->load->view('v_form_tender', $data);
			$this->load->view('v_footer', $data);

		}

	}

	function iklan($id) {
			$tender = $this->m_tender->getTender($id);
			$data['tender'] = $tender[0];
			$data['title'] = 'Tender';

			$this->load->view('v_header', $data);
			$this->load->view('v_iklan_tender', $data);
			$this->load->view('v_footer', $data);
	}

	function view($id) {
		if($this->m_pengguna->check_login()) {

			$user = $this->session->userdata('logged_in');
			$data['user'] = $user;
			$seksyen = $user['seksyen'];
			$data['title'] = 'Tender';

			$tender = $this->m_tender->getTender($id);
			$data['tender'] = $tender[0];
			//print_r($data['tender']);
			$this->load->view('v_header', $data);
			$this->load->view('v_menu', $data);
			$this->load->view('v_view_tender', $data);
			$this->load->view('v_footer', $data);

		}
	}

	function upload() {
		if($this->m_pengguna->check_login()) {
			if($this->input->post()){
		        $config['upload_path'] = './uploads/jadual';
		        $config['allowed_types'] = 'pdf';

		        $this->load->library('upload', $config);

		        if ( ! $this->upload->do_upload('jadual_harga'))
		        {
		          $error = array('error' => $this->upload->display_errors());
		          print_r($error);
		          //redirect(base_url('tender/error'), 'refresh');
		        }
		        else
		        {
		          $id = $this->input->post('tender_id');
		          $upload_data = $this->upload->data('file_name'); 
		          $file_path = $config['upload_path'].'/'.$upload_data['file_name'];
		          
		          if($this->m_tender->updateTenderJadual($id, $upload_data['file_name'])) {

		         	 redirect(base_url('tender/view/'.$id), 'refresh');

		      	  }
		      	  else {
		      	  	echo  'Error';
		      	  }
		          
		        }
		     }
		}
	}

	function generate($id, $no_tender) {
		  
	     
		$this->load->library('mpdfv5/mpdf');

		$mpdf = new mPDF('c', 'A4-L');

		$mpdf->setAutoTopMargin = 'stretch';
		$mpdf->setAutoBottomMargin = 'stretch';
		//$html = base_url('tender/iklan/'.$id);

		//$this->load->helper(array('dompdf', 'file'));

		//Set up Tender
		$tender = $this->m_tender->getTender($id);
		$data['tender'] = $tender[0];
		$data['title'] = 'Tender';

		
		 $html = $this->load->view('v_header', $data, TRUE);
		 $html .= $this->load->view('v_iklan_tender', $data, TRUE);
		 $html .=$this->load->view('v_footer', $data, TRUE);

		 $mpdf->WriteHTML($html);

		 $filepath = './tmp/'.$no_tender.'.pdf';

	     // $data = pdf_create($html, '', false);
    	 // write_file($filepath, $data);

		 $mpdf->Output($filepath);

		// redirect guna javascript sebab ada header sent, itu yang banyak error tu.
		echo '<html><script> location.replace("'.base_url('tmp/'.$no_tender.'.pdf').'"); </script></html>';
	}
}