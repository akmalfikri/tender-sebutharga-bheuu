<?php

class M_tender extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function getAllTenders($jenis = '', $status = '', $perpage = '', $offset = '') {

    		$this->db->select('info_tender.ID as id, info_pegawai.no_telefon as no_tel, info_tender.no_tender, info_tender.jenis_pendaftaran, info_tender.butiran_tender, info_pegawai.nama, info_tender.negeri, info_tender.t_mula_iklan, info_seksyen.nama_seksyen, info_tender.status_tender');
    		$this->db->join('info_seksyen', 'info_seksyen.ID = info_tender.seksyen');
    		$this->db->join('info_pegawai', 'info_pegawai.ID = info_tender.pegawai_bertanggungjawab');
    		$this->db->where('info_tender.status_tender', $status);
            $this->db->limit($perpage, $offset);
            
            if($jenis != 'all') {
    		  $this->db->where('info_tender.jenis_pendaftaran', $jenis);
            }

    		$query = $this->db->get('info_tender');

    		return $query->result_array();
    }

     function getNumTenders($jenis = '', $status = '') {

            $this->db->select('info_tender.ID as id, info_tender.no_tender, info_tender.jenis_pendaftaran, info_tender.butiran_tender, info_pegawai.nama, info_tender.negeri, info_tender.t_mula_iklan, info_seksyen.nama_seksyen, info_tender.status_tender');
            $this->db->join('info_seksyen', 'info_seksyen.ID = info_tender.seksyen');
            $this->db->join('info_pegawai', 'info_pegawai.ID = info_tender.pegawai_bertanggungjawab');
            $this->db->where('info_tender.status_tender', $status);
            
            if($jenis != 'all') {
              $this->db->where('info_tender.jenis_pendaftaran', $jenis);
            }

            $query = $this->db->get('info_tender');

            return $query->num_rows();
    }

    function getPegawai($seksyen) {
            $this->db->where('seksyen', $seksyen);
             $query = $this->db->get('info_pegawai');

            return $query->result_array();
    }

    function getClasses($seksyen) {
            $this->db->where('dept', $seksyen);
            $query = $this->db->get('info_class');

            return $query->result_array();
    }

    function save_tender() {

            $this->db->insert('info_tender', $this->input->post());

    }

    function getTender($id) {
            $this->db->where('id', $id);
            $query = $this->db->get('info_tender');

            return $query->result_array();
    }

    function updateTenderJadual($id, $file_name) {
            $data = array(
                'jadual_harga' => $file_name,
                'status_tender' => '3',
                );

            $this->db->where('ID', $id);
            $this->db->update('info_tender', $data); 

            return TRUE;
    }

}
