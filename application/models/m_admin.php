<?php

class M_admin extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function getDaerahFromNegeri($negeri) {
    	$this->db->where('kod_negeri', $negeri);
    	$query = $this->db->get('info_daerah');

              if($query->result()){
                  foreach ($query->result() as $daerah) {
                     $daerahs[$daerah -> id] = $daerah -> nama_daerah;
                  }
                 return $daerahs;
              }
              else{
                 return 0;
              }
    }

}