<?php

class M_pengguna extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function login() {
    	$this->load->library('encrypt');

    	$username = $this->input->post('username');
    	$password = $this->input->post('password');

    	$this->db->where('username', $username);
    	$query = $this->db->get('info_pengguna');

    	if($query->num_rows() == 1) {
    		$result = $query->result();

    		$passworddb = $result[0]->password;
    		$passwordde = $this->encrypt->decode($passworddb);

    		if($passwordde == $password) {
    			return $result;
    		}
    		else {
    			return FALSE;
    		}
    	}

    	else {
    		return FALSE;
    	}

    }

    function check_login() {
        
        if($this->session->userdata('logged_in')) {
            return TRUE;
        }
        else {
            redirect('login', 'refresh');
        }

    }

    function getAllUsers() {

        $this->db->select('info_pengguna.*, info_seksyen.nama_seksyen');
        $this->db->join('info_seksyen', 'info_seksyen.ID = info_pengguna.seksyen');
        $this->db->order_by('info_pengguna.nama_penuh', 'ASC');
        $query = $this->db->get('info_pengguna');

        return $query->result_array();

    }
    
}