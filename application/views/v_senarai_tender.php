<div class="col-md-12">
	<h2><?php echo $title; ?></h2>

	<ul class="nav nav-pills" role="tablist">
	  <li><a href="<?php echo base_url('tender/senarai/'.$jenis.'/baru'); ?>">Baru</a></li>
	  <li><a href="<?php echo base_url('tender/senarai/'.$jenis.'/iklan'); ?>">Iklan</a></li>
	  <li><a href="<?php echo base_url('tender/senarai/'.$jenis.'/penilaian'); ?>">Penilaian</a></li>
	  <li><a href="<?php echo base_url('tender/senarai/'.$jenis.'/selesai'); ?>">Selesai</a></li>
	  <li><a href="<?php echo base_url('tender/senarai/'.$jenis.'/batal'); ?>">Batal</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">

	<!-- isi perut semua -->
	  <div class="tab-pane active">
	  	<table class="table table-bordered table-hover">
		<thead>
		<tr class="info">
			<th class="text-center">#</th><th class="text-center">No Tender</th><th class="text-center">Butiran Tender</th><th class="text-center">Pegawai</th><th class="text-center">Tarikh Mula</th>
			<th class="text-center">Seksyen</th><th>Jenis</th>
			<?php if($status == 1) { ?>
			<th class="text-center">Tindakan</th>
			<?php } ?>
		</tr>
		</thead>
		<tbody>
			<?php foreach($tender as $key => $b) { ?>
			<tr>
				<td><?php echo $key+1+$offset; ?></td>
				<td><a href="<?php echo base_url('tender/view/'.$b['id']); ?>"><?php echo $b['no_tender']; ?></a></td>
				<td><?php echo $b['butiran_tender']; ?></td>
				<td><?php echo $b['nama']; ?></td>
				<td><?php echo $b['t_mula_iklan']; ?></td>
				<td><?php echo $b['nama_seksyen']; ?></td>
				<td><?php echo $b['jenis_pendaftaran']; ?></td>

				<?php if($b['status_tender'] == '1') { ?>
				<td><a href="<?php echo base_url('tender/edit/'.$b['id']); ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span> Kemaskini</a></td>
				<?php } ?>
			</tr>
			<?php } ?>
		</tbody>
		</table>
		<?php echo $paging; ?>
	  </div>
	  <!-- habis baru -->

	</div>

	
</div>