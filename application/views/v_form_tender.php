<div class="col-md-12">

<h2><?php echo $title; ?></h2>

    <form method="post" role="form" accept-charset="utf-8" action="<?php echo base_url('tender/baru'); ?>" />
      
      <fieldset>
      <legend>Butiran Umum</legend>

      <div class="form-group">
        <select name="jenis_pendaftaran" id="jenis_pendaftaran" class="form-control">
        <option value='-1'>- Sila Pilih Jenis Pendaftaran -</option>
        <option value='tender'>Tender</option>
        <option value='sebutharga'>Sebutharga</option>
        <option value='ebidding'>EBidding</option>
        </select>
      </div>

      <div class="form-group">
        <label for="no_tender">Nombor Tender / Sebutharga</label>
        <input name="no_tender" class="form-control" id='no_tender'>
      </div>

      <div class="form-group">
        <label for="butiran_tender">Butiran Tender / Sebutharga</label>
        <textarea name="butiran_tender" class="form-control" id='butiran_tender'></textarea>
      </div>

      <div class="form-group">
        <label for="negeri">Negeri</label>
        <?php echo state_dropdown('negeri', '', 'negeri', 'form-control') ?>
      </div>

      <div class="form-group">
        <label for="harga_dokumen">Harga Dokumen</label>
        <input name="harga_dokumen" class="form-control" id='harga_dokumen'>
      </div>

      <div class="form-group">
        <label for="anggaran_harga">Anggaran Harga</label>
        <input name="anggaran_harga" class="form-control" id='anggaran_harga'>
      </div>

      <div class="form-group">
        <select name="pegawai_bertanggungjawab" id="pegawai_bertanggungjawab" class="chosen-select">
            <option value="0">-- Sila Pilih Pegawai Bertanggungjawab --</option>
            <?php foreach($officers as $off) { ?>
            <option value="<?php echo $off['ID']; ?>"><?php echo $off['nama']; ?></option>
            <?php } ?>
          </select><br /><br />
          <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myPegawai">
            Tambah Pegawai
          </a>
      </div>

      <div class="form-group">
        <select name="kelas_pendaftaran" multiple id="kelas_pendaftaran" class="chosen-select">
            <option value="0">-- Sila Pilih Kelas Pendaftaran --</option>
            <?php foreach($classes as $kelas) { ?>
            <option value="<?php echo $kelas['ID']; ?>"><?php echo $kelas['kod'].' - '.$kelas['nama']; ?></option>
            <?php } ?>
        </select><br /><br />
        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myClass">
            Tambah Kelas
        </a>
      </div>
      </fieldset>

      <fieldset>
      <legend>Butiran Iklan</legend>
        <div class="form-group">
          <label for="t_mula_iklan">Tarikh Mula Iklan</label>
          <input name="t_mula_iklan" class="form-control" type="date" id='t_mula_iklan'>
        </div>
        <div class="form-group">
          <label for="t_tutup_iklan">Tarikh Tutup Iklan</label>
          <input name="t_tutup_iklan" class="form-control" type="date" id='t_tutup_iklan'>
        </div>
      </fieldset>

      <fieldset>
      <legend>Butiran Taklimat</legend>
        <div class="form-group">
          <label for="t_taklimat">Tarikh Taklimat</label>
          <input name="t_taklimat" class="form-control" type="date" id='t_taklimat'>
        </div>
        <div class="form-group">
          <label for="masa_taklimat">Masa Taklimat</label>
          <input name="masa_taklimat" class="form-control" id='masa_taklimat'>
        </div>
        <div class="form-group">
          <label for="tempat_taklimat">Tempat Taklimat</label>
          <input name="tempat_taklimat" class="form-control" id='tempat_taklimat'>
        </div>
      </fieldset>

      <fieldset>
      <legend>Butiran Lawatan</legend>
        <div class="form-group">
          <label for="t_lawatan">Tarikh Lawatan</label>
          <input name="t_lawatan" class="form-control" type="date" id='t_lawatan'>
        </div>
        <div class="form-group">
          <label for="masa_lawatan">Masa Lawatan</label>
          <input name="masa_lawatan" class="form-control" id='masa_lawatan'>
        </div>
        <div class="form-group">
          <label for="tempat_lawatan">Tempat Lawatan</label>
          <input name="tempat_lawatan" class="form-control" id='tempat_lawatan'>
        </div>
      </fieldset>
      <div class="form-group">
      <input type="hidden" name='seksyen' value="<?php echo $user['seksyen']; ?>" class="btn btn-primary btn-lg">
      <input type="submit" value="Simpan" class="btn btn-primary btn-lg">
      </div>
   </form>
</div>


<!-- Modal -->
<div class="modal" id="myPegawai" tabindex="-1" role="dialog" aria-labelledby="myPegawaiLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myPegawaiLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        Cubaan
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Class-->
<div class="modal" id="myClass" tabindex="-1" role="dialog" aria-labelledby="myClassLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myClassLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        Cubaan
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<script>
  CKEDITOR.replace( 'butiran_tender' );
</script>