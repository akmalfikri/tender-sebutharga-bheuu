<div class="col-md-10 col-md-offset-1">
	<h2><?php echo $title; ?></h2>

	
	<table class="table table-bordered table-hover">
		<thead>
		<tr class="info">
			<th class="text-center">#</th><th class="text-center">Nama Penuh</th><th class="text-center">Username</th><th class="text-center">Seksyen</th>
			<th class="text-center">Peranan</th><th class="text-center">Status</th><th class="text-center">Tindakan</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($pengguna as $key => $p) { ?>
			<tr>
				<td><?php echo $key+1; ?></td>
				<td><a href="<?php echo base_url('admin/pengguna/view'.$p['id']); ?>"><?php echo $p['nama_penuh']; ?></a></td>
				<td><?php echo $p['username']; ?></td>
				<td><?php echo $p['nama_seksyen']; ?></td>
				<td><?php echo getRoleName($p['role']); ?></td>
				<td><?php echo getStatus($p['status_pengguna']); ?></td>
				<td><a href="<?php echo base_url('admin/pengguna/edit/'.$p['id']); ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span> Kemaskini</a></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>