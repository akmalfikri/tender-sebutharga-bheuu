<div class="col-md-6 col-md-offset-3">
<h2><?php echo $title; ?></h2>
		
	<form method="POST">
	  <div class="form-group">
        <label for="negeri">Negeri</label>
        <?php echo state_dropdown('negeri', '', 'negeri', 'form-control', 'onChange="getDaerahList(this.value);"') ?>
      </div>
      <div class="col-md-5" id="daerah">
      </div>
    </form>

</div>

<script>

function getDaerahList(negeri) {
  var ajaxRespond = "<?=site_url().'admin/get_daerah_from_negeri/'?>" + negeri;

  $.ajax({
        type: "POST",
        url: ajaxRespond,
        cache: false,
        dataType:"html",
        beforeSend:function(){
              // this is where we append a loading image
              $("#loading").html('<div class="load"><img src="<?php echo base_url("assets/images/load.gif")?> width="16px" height="16px"></div>');
        },
        success: function(data){
              $("#daerah").html(data);
              $("#loading").empty();
        },
    });
}

</script>