<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url('main'); ?>">BHEUU</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <?php 
        // If user is admin
        if($user['role'] == 1) { ?>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pentadbiran <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<?php echo base_url('admin/pengguna'); ?>">Pengguna</a></li>
            <li><a href="<?php echo base_url('admin/pengguna/baru'); ?>">Daftar Pengguna Baru</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Penyelenggaraan <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<?php echo base_url('admin/kod'); ?>">Kod</a></li>
            <li><a href="<?php echo base_url('admin/kod/baru'); ?>">Daftar Kod Baru</a></li>
            <li><a href="<?php echo base_url('admin/daerah'); ?>">Daerah</a></li>
          </ul>
        </li>

        <?php } ?>

        <?php 
        // If user is pendaftar
        if($user['role'] == 2) { ?>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tender <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<?php echo base_url('tender/senarai/tender/baru'); ?>">Senarai Tender</a></li>
            <li><a href="<?php echo base_url('tender/senarai/sebutharga/baru'); ?>">Senarai Sebutharga</a></li>
            <li><a href="<?php echo base_url('tender/senarai/ebidding/baru'); ?>">Senarai Ebidding</a></li>
            <li><a href="<?php echo base_url('tender/baru'); ?>">Tambah Tender</a></li>
          </ul>
        </li>

        <?php } ?>

        <?php 
        // If user is pelulus
        if($user['role'] == 3) { ?>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tender <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<?php echo base_url('tender/senarai'); ?>">Senarai</a></li>
            <li><a href="<?php echo base_url('tender/laporan'); ?>">Laporan</a></li>
          </ul>
        </li>

        <?php } ?>

      </ul>
      <!--<form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>-->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Welcome back, <?php echo $user['username']; ?></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Change Password</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url('main/logout'); ?>">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>