<div class="col-md-12 tender">
	<div class="col-md-3">
		Logo di sini
	</div>
	<div class="col-md-9"><h2>BAHAGIAN HAL EHWAL UNDANG - UNDANG<br />
	JABATAN PERDANA MENTERI</h2></div>
	<div class="col-md-12">
		<span class="subheader col-md-12 text-center">KENYATAAN <?php echo convertjenis($tender['jenis_pendaftaran']); ?></span>
		<p>Sebut harga ini adalah dipelawa kepada <strong>Kontraktor Bumiputera</strong> di <?php echo abbr_to_name($tender['negeri']); ?></p>
	</div>
	<div class="col-md-12">
		<table class="table table-bordered">
			<thead>
				<tr class="info">
					<th>Bil</th>
					<th>Butir - butir tawaran</th>
					<th>Harga Dokumen tawaran</th>
					<th>Syarat pendaftaran</th>
					<th>Tempat dan Tarikh Lawatan Tapak</th>
					<th>Tarikh Tawaran Ditutup</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td><?php echo $tender['butiran_tender']; ?></td>
					<td><?php echo $tender['harga_dokumen']; ?></td>
					<td><?php echo $tender['kelas_pendaftaran']; ?></td>
					<td><strong>Tempat : </strong><?php echo $tender['tempat_lawatan']; ?><br />
						<strong>Tarikh : </strong><?php echo $tender['t_lawatan']; ?><br />
						<strong>Masa  :	</strong><?php echo $tender['masa_lawatan']; ?><br />
					</td>
					<td><strong>Tarikh Tawaran Ditutup : </strong><?php echo $tender['t_tutup_iklan']; ?><br />
						<strong>Masa  :	</strong><?php echo $tender['masa_lawatan']; ?><br />
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12">
		<p>Peringatan : Ayat2 cinta</p>
		<p>Maklumat Pegawai - <?php echo $tender['pegawai_bertanggungjawab']; ?></p>
	</div>
</div>