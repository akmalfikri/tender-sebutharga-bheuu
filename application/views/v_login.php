      <div class="col-sm-6 col-md-4 col-md-offset-4">

        <h1><?php echo $title; ?></h1>
        <?php echo $message; ?>
      <?php 
      $attributes = array('id' => 'login', 'role'=> 'form');

      echo form_open('login/validate', $attributes);
      ?>

      <div class="form-group">
        <label for="username">Username</label>
        <?php echo form_input(array('name' => 'username', 'class' => 'form-control', 'placeholder' => 'Username', 'required' => TRUE)); ?>
      </div>

      <div class="form-group">
        <label for="password">Password</label>
        <?php echo form_password(array('name' => 'password', 'class' => 'form-control', 'placeholder' => 'Password',  'required' => TRUE)); ?>
      </div>

      <div class="form-group">
        <?php echo form_submit(array('name' => 'submit', 'class' => 'btn btn-lg btn-primary btn-block', 'value' => 'Log Masuk')); ?>
      </div>

      <?php echo form_close(); ?>

      </div>