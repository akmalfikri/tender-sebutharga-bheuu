<div class="col-md-12">
	<!-- Butiran -->
	<div class="col-md-8 mid-border">
		<h4>No Tender</h4>
		<p><?php echo $tender['no_tender']; ?></p>
		<h4>Butiran Tender</h4>
		<p><?php echo $tender['butiran_tender']; ?></p>
		<h4>Pegawai Bertanggungjawab</h4>
		<p><?php echo $tender['pegawai_bertanggungjawab']; ?></p>
		<h4>No Telefon Pegawai</h4>
		<p><?php echo $tender['no_tel']; ?></p>
		<h4>Negeri</h4>
		<p><?php echo abbr_to_name($tender['negeri']); ?></p>
		<h4>Seksyen</h4>
		<p><?php echo $tender['seksyen']; ?></p>
		<h4>Harga Dokumen</h4>
		<p>RM <?php echo $tender['harga_dokumen']; ?></p>
		<h4>Seksyen</h4>
		<p><?php echo $tender['seksyen']; ?></p>

	</div>

	<!-- Kotak status -->
	<div class="col-md-4 kotak-status">
		<?php if($tender['status_tender'] == '1') { ?>
		<h3><a href="<?php echo base_url('tender/edit/'.$tender['ID']); ?>" class="btn btn-info">Kemaskini</a></h3>
		<?php } ?>
		<h3>STATUS : <?php echo getTenderStatus($tender['status_tender']); ?></h3>

		<h3>IKLAN : </h3>
			<a href="<?php echo base_url('tender/iklan/'.$tender['ID']); ?>" class="btn btn-success">Papar Iklan</a>
			<a href="<?php echo base_url('tender/generate/'.$tender['ID'].'/'.$tender['no_tender']); ?>" class="btn btn-success">Muat turun Iklan</a>
		
		<?php if($tender['status_tender'] >= '2') { ?>

		<h3>JADUAL HARGA : </h3>

			<?php if($tender['jadual_harga'] == '') { ?>
				<a href="#" class="btn btn-small btn-primary" data-toggle="modal" data-target="#myHarga">Muat naik Jadual Harga</a>
			<?php } else { ?>
				<a href="<?php echo base_url('uploads/jadual/'.$tender['jadual_harga']); ?>" class="btn btn-small btn-success">Papar Jadual Harga</a>
		<?php } } ?>

		<?php if($tender['status_tender'] >= '3') { ?>

		<h3>KEPUTUSAN : </h3>

			<?php if($tender['status_tender'] == '3') { ?>
				<a href="#" class="btn btn-small btn-primary" data-toggle="modal" data-target="#myKeputusan">Masukkan keputusan</a>
			<?php } else { ?>
				<a href="#" class="btn btn-small btn-primary" data-toggle="modal" data-target="#myPaparKeputusan">Papar keputusan</a>
		<?php } } ?>
	</div>
</div>

<!-- Modal Harga-->
<div class="modal" id="myHarga" tabindex="-1" role="dialog" aria-labelledby="myHargaLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myHargaLabel">Muat naik Jadual Harga</h4>
      </div>
      <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('tender/upload'); ?>">
      <div class="modal-body">
        	<div class="form-group">
        		<label for="jadual_harga">Muat naik</label>
        		<input type="file" name="jadual_harga" id="jadual_harga">
        	</div>
      </div>
      <input type="hidden" name="tender_id" value="<?php echo $tender['ID']; ?>">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" value="Muat naik">
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Keputusan-->
<div class="modal" id="myKeputusan" tabindex="-1" role="dialog" aria-labelledby="myKeputusanLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myKeputusanLabel">Muat naik Jadual Harga</h4>
      </div>
      <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('tender/upload'); ?>">
      <div class="modal-body">
        	Borang Keputusan
      </div>
      <input type="hidden" name="tender_id" value="<?php echo $tender['ID']; ?>">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" value="Muat naik">
      </div>
      </form>
    </div>
  </div>
</div>