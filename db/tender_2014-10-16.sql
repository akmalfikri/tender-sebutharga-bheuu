# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.20)
# Database: tender
# Generation Time: 2014-10-16 08:41:41 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`)
VALUES
	('df0afda2a386942354d8a62ef6e355be','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.101 Safari/537.3',1413448493,'a:2:{s:9:\"user_data\";s:0:\"\";s:9:\"logged_in\";a:4:{s:2:\"id\";s:1:\"2\";s:8:\"username\";s:9:\"pendaftar\";s:4:\"role\";s:1:\"2\";s:7:\"seksyen\";s:1:\"1\";}}');

/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table info_class
# ------------------------------------------------------------

DROP TABLE IF EXISTS `info_class`;

CREATE TABLE `info_class` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `dept` varchar(255) NOT NULL,
  `kod` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `info_class` WRITE;
/*!40000 ALTER TABLE `info_class` DISABLE KEYS */;

INSERT INTO `info_class` (`ID`, `nama`, `dept`, `kod`)
VALUES
	(1,'Cubaan','1','P45');

/*!40000 ALTER TABLE `info_class` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table info_keputusan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `info_keputusan`;

CREATE TABLE `info_keputusan` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sykt` varchar(255) NOT NULL,
  `harga_tawaran` varchar(255) NOT NULL,
  `tempoh_bekalan` varchar(255) NOT NULL,
  `t_JPM` date NOT NULL,
  `t_SST` date NOT NULL,
  `t_jawab_SST` date NOT NULL,
  `t_created` date NOT NULL,
  `t_updated` date NOT NULL,
  `id_create` int(11) NOT NULL,
  `id_update` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table info_pegawai
# ------------------------------------------------------------

DROP TABLE IF EXISTS `info_pegawai`;

CREATE TABLE `info_pegawai` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `no_telefon` varchar(255) NOT NULL,
  `seksyen` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `info_pegawai` WRITE;
/*!40000 ALTER TABLE `info_pegawai` DISABLE KEYS */;

INSERT INTO `info_pegawai` (`ID`, `nama`, `no_telefon`, `seksyen`)
VALUES
	(1,'Saiful','0143375250',1);

/*!40000 ALTER TABLE `info_pegawai` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table info_pengguna
# ------------------------------------------------------------

DROP TABLE IF EXISTS `info_pengguna`;

CREATE TABLE `info_pengguna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_penuh` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `seksyen` int(11) NOT NULL,
  `status_pengguna` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `info_pengguna` WRITE;
/*!40000 ALTER TABLE `info_pengguna` DISABLE KEYS */;

INSERT INTO `info_pengguna` (`id`, `username`, `password`, `nama_penuh`, `email`, `seksyen`, `status_pengguna`, `role`)
VALUES
	(1,'admin','p/OhDJ7wMeJXxGNyCTgkAqj8XM1RVfLOdE2O+syKK516/AiTBkJM9TBamhRwEWjnLyCn9FDtjHbWZODtEJl+Xw==','Admin','akmalfikri@me.com',1,1,1),
	(2,'pendaftar','p/OhDJ7wMeJXxGNyCTgkAqj8XM1RVfLOdE2O+syKK516/AiTBkJM9TBamhRwEWjnLyCn9FDtjHbWZODtEJl+Xw==','Pendaftar','pendaftar@bheuu.com',1,1,2),
	(3,'pelulus','p/OhDJ7wMeJXxGNyCTgkAqj8XM1RVfLOdE2O+syKK516/AiTBkJM9TBamhRwEWjnLyCn9FDtjHbWZODtEJl+Xw==','Pelulus','pelulus@bheuu.com',2,1,3);

/*!40000 ALTER TABLE `info_pengguna` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table info_seksyen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `info_seksyen`;

CREATE TABLE `info_seksyen` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `kod_seksyen` varchar(255) NOT NULL,
  `nama_seksyen` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `info_seksyen` WRITE;
/*!40000 ALTER TABLE `info_seksyen` DISABLE KEYS */;

INSERT INTO `info_seksyen` (`ID`, `kod_seksyen`, `nama_seksyen`)
VALUES
	(1,'SPM','SEKSYEN PENGURUSAN MAKLUMAT'),
	(2,'SP','SEKSYEN PEMBANGUNAN'),
	(3,'SK','SEKSYEN KEWANGAN');

/*!40000 ALTER TABLE `info_seksyen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table info_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `info_status`;

CREATE TABLE `info_status` (
  `ID` int(11) NOT NULL,
  `kod` varchar(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `info_status` WRITE;
/*!40000 ALTER TABLE `info_status` DISABLE KEYS */;

INSERT INTO `info_status` (`ID`, `kod`, `nama`)
VALUES
	(1,'B','BARU'),
	(2,'I','IKLAN'),
	(3,'P','PENILAIAN'),
	(4,'S','SELESAI'),
	(5,'B','BATAL');

/*!40000 ALTER TABLE `info_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table info_tender
# ------------------------------------------------------------

DROP TABLE IF EXISTS `info_tender`;

CREATE TABLE `info_tender` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `no_tender` varchar(255) NOT NULL,
  `butiran_tender` varchar(255) NOT NULL,
  `negeri` varchar(11) NOT NULL DEFAULT '',
  `harga_dokumen` varchar(255) NOT NULL,
  `pegawai_bertanggungjawab` varchar(11) NOT NULL DEFAULT '',
  `kelas_pendaftaran` varchar(255) NOT NULL,
  `t_mula_iklan` date NOT NULL,
  `t_tutup_iklan` date NOT NULL,
  `t_taklimat` date NOT NULL,
  `masa_taklimat` varchar(255) NOT NULL,
  `tempat_taklimat` varchar(255) NOT NULL,
  `t_lawatan` date NOT NULL,
  `masa_lawatan` varchar(255) NOT NULL,
  `tempat_lawatan` varchar(255) NOT NULL,
  `seksyen` varchar(11) DEFAULT '',
  `anggaran_harga` varchar(255) NOT NULL,
  `jenis_pendaftaran` varchar(255) NOT NULL,
  `jadual_harga` varchar(255) DEFAULT '',
  `status_tender` int(11) NOT NULL DEFAULT '1',
  `dt_dreated` date DEFAULT NULL,
  `dt_updated` date DEFAULT NULL,
  `id_created` int(11) DEFAULT NULL,
  `id_updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `info_tender` WRITE;
/*!40000 ALTER TABLE `info_tender` DISABLE KEYS */;

INSERT INTO `info_tender` (`ID`, `no_tender`, `butiran_tender`, `negeri`, `harga_dokumen`, `pegawai_bertanggungjawab`, `kelas_pendaftaran`, `t_mula_iklan`, `t_tutup_iklan`, `t_taklimat`, `masa_taklimat`, `tempat_taklimat`, `t_lawatan`, `masa_lawatan`, `tempat_lawatan`, `seksyen`, `anggaran_harga`, `jenis_pendaftaran`, `jadual_harga`, `status_tender`, `dt_dreated`, `dt_updated`, `id_created`, `id_updated`)
VALUES
	(1,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(2,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(3,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(4,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(5,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(6,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(7,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(8,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(9,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(10,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(11,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(12,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(13,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(14,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(15,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(16,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(17,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(18,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(19,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(20,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(21,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(22,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(23,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(24,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(25,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(26,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(27,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(28,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(29,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(30,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(31,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(32,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(33,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(34,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(35,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(36,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(37,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(38,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(39,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',4,'2014-10-15','2014-10-15',20141015,20141015),
	(40,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(41,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(42,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(43,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',4,'2014-10-15','2014-10-15',20141015,20141015),
	(44,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',4,'2014-10-15','2014-10-15',20141015,20141015),
	(45,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(46,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(47,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(48,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(49,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(50,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(51,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',1,'2014-10-15','2014-10-15',20141015,20141015),
	(52,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(53,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(54,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(55,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(56,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(57,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(58,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(59,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(60,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(61,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(62,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(63,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(64,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',4,'2014-10-15','2014-10-15',20141015,20141015),
	(65,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(66,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(67,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(68,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',4,'2014-10-15','2014-10-15',20141015,20141015),
	(69,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',4,'2014-10-15','2014-10-15',20141015,20141015),
	(70,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',3,'2014-10-15','2014-10-15',20141015,20141015),
	(71,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(72,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(73,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(74,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(75,'1234-6','Cubaan','SN','20.00','1','','2014-10-15','2014-11-15','2014-10-16','8.00 pagi','Pejabat BHEEUU','2014-10-16','10.00 pagi','Pejabat BHEUU','2','30000','tender','',2,'2014-10-15','2014-10-15',20141015,20141015),
	(76,'ABC1234','<p><strong>Cubaan</strong></p>','SL','20','1','1','2014-12-05','2014-12-05','2014-12-05','8.00','BHEUU','2014-05-12','8.00','BHEUU','1','20','sebutharga','',1,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `info_tender` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jenis_daftar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_daftar`;

CREATE TABLE `jenis_daftar` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `kod_daftar` varchar(255) NOT NULL,
  `nama_daftar` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `jenis_daftar` WRITE;
/*!40000 ALTER TABLE `jenis_daftar` DISABLE KEYS */;

INSERT INTO `jenis_daftar` (`ID`, `kod_daftar`, `nama_daftar`)
VALUES
	(1,'T','TENDER'),
	(2,'S','SEBUTHARGA'),
	(3,'E','E-BIDDING');

/*!40000 ALTER TABLE `jenis_daftar` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
